import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
import javax.servlet.annotation.*;

@WebServlet("/insertAvail")
public class insertAvail extends HttpServlet {  
 
   @Override
   public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection conn = null;
		Statement stmt = null;
		
		String userID = request.getParameter("userID").toString();
		String groupID = request.getParameter("groupID").toString();
		String groupName = request.getParameter("groupName").toString();
		String firstName = request.getParameter("firstName").toString();
		String lastName = request.getParameter("lastName").toString();
		String email = request.getParameter("email").toString();
		String times[];
		times= request.getParameterValues("times");
		String[] avail = new String[84];
		String results="";
		//import timeslots
		String timeslots = "D:\\myproject\\tomcat\\webapps\\NinerConnect\\timeslots.txt";
		BufferedReader in = new BufferedReader(new FileReader(timeslots));
		while (in.ready()) {
			for(int i=0; i<avail.length; i++){
					avail[i] = in.readLine();
					}
		}
		
			boolean foundSwitch = false;
				
				//loop through possible available times
				for(int i=0; i<avail.length; i++)
				{
					//loop through selected available times
					for(int j=0; j<times.length; j++)
					{
						//compare possible times to selected times
						if(avail[i].equals(times[j]))
						{
							foundSwitch = true;
							//set timeslot to 1
							avail[i] = "1";
							}
					}
					if(foundSwitch==false)
					{
						//set timeslot to 0
						avail[i] = "0";
						}
						//set foundSwitch back to false
						foundSwitch = false;
				 }
				
				//results to put into database
				for(int i=0; i<avail.length; i++)
				{
					results += avail[i];
					}
			
		
			try
			{
				conn = DriverManager.getConnection("jdbc:mysql://localhost:8888/planit", "root", "dharma");
				stmt = conn.createStatement();
				
				int uID = Integer.parseInt(userID) + 1;
				userID = Integer.toString(uID);
				
				//import javascript function to validate form
				String fileLoc = "D:\\myproject\\tomcat\\webapps\\NinerConnect\\js\\validateForm.txt";
				BufferedReader input = new BufferedReader(new FileReader(fileLoc));
				while (input.ready()) {
					String jsCode = input.readLine();
					out.println(jsCode);
				}
				
				//insert the new group member's info into database
				String values = "'" + userID + "'" + "," + "'" + firstName + "'" + "," + "'" + lastName + "'" + "," + "'" + email + "'"  + "," + "'" + groupName + "'" + "," + "'" + results + "'" + "," + "'" + groupID + "'";
				String query = "INSERT INTO user values(" + values + ");";
				int rsUpdate = stmt.executeUpdate(query);
				
				//form to update the current member's availability
				out.println("<html><link href='css/bootstrap.css' rel='stylesheet'><center><head><title>Group's ID</title></head><body>");
	         out.println("<br><br><h2>Enter your availability</h2>");
				out.println("<form action='updateAvail' method='POST'>");
				out.println("<input type='hidden' value='" + groupID + "' name='groupID' />");
				out.println("<input type='hidden' value='" + groupName + "' name='groupName' />");
				out.println("<input type='hidden' value='" + firstName + "' name='firstName' />");
				out.println("<input type='hidden' value='" + lastName + "' name='lastName' />"); 
				out.println("<input type='hidden' value='" + email + "' name='email' />"); 
				//display table of times for users to input
				out.println("<table  border='1'  align='center'>");
				out.println("<tr>");
				out.println("<td align='center'>Times</td>");
				out.println("<td align='center'>Monday</td>");
				out.println("<td align='center'>Tuesday</td>");
				out.println("<td align='center'>Wednesday</td>");
				out.println("<td align='center'>Thursday</td>");
				out.println("<td align='center'>Friday</td>");
				out.println("<td align='center'>Saturday</td>");
				out.println("<td align='center'>Sunday</td>");
				out.println("</tr>");
				out.println("<tr>");
				out.println("<td>9:00am - 10:00am</td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='00'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='10'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='20'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='30'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='40'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='50'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='60'/></td>");
				out.println("</tr>");
				out.println("<tr>");
				out.println("<td>10:00am - 11:00am</td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='01'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='11'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='21'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='31'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='41'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='51'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='61'/></td>");
				out.println("</tr>");
				out.println("<tr>");
				out.println("<td>11:00am - 12:00pm</td>"); 
				out.println("<td align='center'><input type='checkbox' name='times' value='02'/></td>");
				out.println("<td align='center'><input type='checkbox' name='times' value='12'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='22'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='32'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='42'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='52'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='62'/></td>");
				out.println("                </tr>");
				out.println("                <tr>");
				out.println("                <td>12:00pm - 1:00pm</td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='03'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='13'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='23'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='33'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='43'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='53'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='63'/></td>");
				out.println("                </tr>");
				out.println("                <tr>");
				out.println("                <td>1:00pm - 2:00pm</td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='04'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='14'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='24'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='34'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='44'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='54'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='64'/></td>");
				out.println("               </tr>");
				out.println("                <tr>");
				out.println("                <td>2:00pm - 3:00pm</td>"); 
				out.println("                <td align='center'><input type='checkbox' name='times' value='05'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='15'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='25'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='35'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='45'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='55'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='65'/></td>");
				out.println("                </tr>");
				out.println("                <tr>");
				out.println("                <td>3:00pm - 4:00pm</td>"); 
				out.println("                <td align='center'><input type='checkbox' name='times' value='06'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='16'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='26'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='36'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='46'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='56'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='66'/></td>");
				out.println("                </tr>");
				out.println("                <tr>");
				out.println("                <td>4:00pm - 5:00pm</td>"); 
				out.println("                <td align='center'><input type='checkbox' name='times' value='07'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='17'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='27'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='37'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='47'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='57'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='67'/></td>");
				out.println("                </tr>");
				out.println("                <tr>");
				out.println("                <td>5:00pm - 6:00pm</td>"); 
				out.println("                <td align='center'><input type='checkbox' name='times' value='08'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='18'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='28'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='38'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='48'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='58'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='68'/></td>");
				out.println("                </tr>");
				out.println("                <tr>");
				out.println("                <td>6:00pm - 7:00pm</td>"); 
				out.println("                <td align='center'><input type='checkbox' name='times' value='09'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='19'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='29'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='39'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='49'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='59'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='69'/></td>");
				out.println("                </tr>");
				out.println("                <tr>");
				out.println("                <td>7:00pm - 8:00pm</td>"); 
				out.println("                <td align='center'><input type='checkbox' name='times' value='010'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='110'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='210'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='310'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='410'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='510'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='610'/></td>");
				out.println("                </tr>");
				out.println("                <tr>");
				out.println("                <td>8:00pm - 9:00pm</td>"); 
				out.println("                <td align='center'><input type='checkbox' name='times' value='011'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='111'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='211'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='311'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='411'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='511'/></td>");
				out.println("                <td align='center'><input type='checkbox' name='times' value='611'/></td>");
				out.println("                </tr>");
				out.println("             </table>");
				//end table
				

				out.println("<br><br><button class='btn btn-large btn-success' type='submit'>Update your availability</button>");
				out.println("</form>");
				
				//link to current member's group page
				out.println("<form action='GroupAvail' method='POST'>");
				out.println("<input type='hidden' value='" + groupID + "' name='groupID' />");
				out.println("<button class='btn btn-custom' type='submit'>Go to your group's page</button>");
				out.println("</form>");
	         out.println("</body></html>");
				

					
			} catch (Exception e) 
			{
				e.printStackTrace();
				System.err.println("Exception: " + e.getMessage());
			} 
			finally 
			{
				try 
				{
					if (conn != null) 
					{
						conn.close();
					}
				} 
				catch (SQLException e)
				{
				}               
			}
		}
    }