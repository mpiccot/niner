import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.util.*;
 
public class GroupCreate extends HttpServlet {  
 
   @Override
   public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection conn = null;
		Statement stmt = null;
		
		String groupName = request.getParameter("groupName").toString();
		String lFirstName = request.getParameter("leaderFirstName").toString();
		String lLastName = request.getParameter("leaderLastName").toString();
		String email = request.getParameter("leaderEmail").toString();
		String avail = " ";
		
			try
			{
				conn = DriverManager.getConnection("jdbc:mysql://localhost:8888/planit", "root", "dharma");
				stmt = conn.createStatement();
				
				int gID, uID = 0;
				String groupID, userID = null;
			
				Random idGen = new Random();
			
				do
				{
					gID = idGen.nextInt(9999);
					groupID = Integer.toString(gID);
					uID = idGen.nextInt(9999);
					userID = groupID + Integer.toString(uID);	
				}while(groupID == userID);
				
				Class.forName("com.mysql.jdbc.Driver").newInstance();
				
				String values = "'" + userID + "'" + "," + "'" + lFirstName + "'" + "," + "'" + lLastName + "'" + "," + "'" + email + "'"  + "," + "'" + groupName + "'" + "," + "'" + avail + "'" + "," + "'" + groupID + "'";
				String query = "INSERT INTO user values(" + values + ");";
				int rsUpdate = stmt.executeUpdate(query);
				
				out.println("<html><link href='css/bootstrap.css' rel='stylesheet'><center><head><title>Group's ID</title></head><body>");
				out.println("<br><br><h2>Here's your group's ID number.</h2><h4>Remember it and tell the rest of the group:</h4>");
				out.println("<form action='updateAvail' method='POST'>");
				out.println("<input style='text-align:center; font-size:30px;' value='" + groupID + "' name='groupID' readonly/>");
				out.println("<input type='hidden' value='" + groupName + "' name='groupName' />");
				out.println("<input type='hidden' value='" + lFirstName + "' name='firstName' />");
				out.println("<input type='hidden' value='" + lLastName + "' name='lastName' />"); 
				out.println("<input type='hidden' value='" + email + "' name='email' />"); 
				out.println("<input type='hidden' value='" + avail + "' name='times' />"); 
				out.println("<br><br><button class='btn btn-large btn-success' type='submit'>Enter your available times</button>");
				out.println("</form>");

				

	         out.println("</body></html>");
					
			} catch (Exception e) 
			{
				e.printStackTrace();
				System.err.println("Exception: " + e.getMessage());
			} 
			finally 
			{
				try 
				{
					if (conn != null) 
					{
						conn.close();
					}
				} 
				catch (SQLException e)
				{
				}               
			}
		}
    }