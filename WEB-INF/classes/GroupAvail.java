import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.util.*;

@WebServlet("/GroupAvail")
public class GroupAvail extends HttpServlet {  
 
   public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
	{
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		Connection conn = null;
		Statement stmt = null;
		String availability = " ";
		
		String groupID = request.getParameter("groupID").toString();
		boolean groupExist = groupExists(groupID);
		
		//check if group's already in database, if doesn't exist display this error message
		if(!groupExist)
		{
			out.println("<html><link href='css/bootstrap.css' rel='stylesheet'><center><head><title>Group Doesn't Exist</title></head><body>");
			out.println("<br><br><h3>Oops, the group ID you entered is invalid.</h3>");
			out.println("<a href=" + "'" + "../NinerConnect/planit.html" + "'" + "><h4>Return to Previous Page</h4></a>");
		}
		
		else
		{			
			try
			{
				conn = DriverManager.getConnection("jdbc:mysql://localhost:8888/planit", "root", "dharma");
				stmt = conn.createStatement();
				String jsCode, navCode, groupName = "";
				String lastUserID = " ";
				
				
				out.println("<!DOCTYPE html><html><link href='css/bootstrap.css' rel='stylesheet'><head></head>");
				out.println("<title>Group Page</title>");
				
				//imports the top navigation
				String navLoc = "D:\\myproject\\tomcat\\webapps\\NinerConnect\\navi.html";
				BufferedReader navIn = new BufferedReader(new FileReader(navLoc));
				while (navIn.ready()) {
					navCode = navIn.readLine();
					out.println(navCode);
				}
				
				//import javascript function to validate form
				String fileLoc = "D:\\myproject\\tomcat\\webapps\\NinerConnect\\js\\validateForm.txt";
				BufferedReader in = new BufferedReader(new FileReader(fileLoc));
				while (in.ready()) {
					jsCode = in.readLine();
					out.println(jsCode);
				}
				//query the group name
				String gquery = "select * from user where groupID='" + groupID + "'";
				ResultSet group = stmt.executeQuery(gquery);
				while(group.next())
				{
					groupName = group.getString("teamName");
				}
				out.println("<body><div class='row-fluid'>");
				out.println("<div class='span5'><h3>Current members in <span class='color-highlight2'>" + groupName + "</span> (ID:" + groupID +")</h3>");
				
				//Execute a SQL SELECT query
				String query = "select * from user where groupID='" + groupID + "'";
				ResultSet rset = stmt.executeQuery(query);
				
				//initialize student object
				Student std = new Student();
				
				//final matrix
				int[][] finalMatrix=new int[7][12];
				for(int i = 0; i < 7; i++)
				{
					for(int j=0; j < 12; j++)
					{
						finalMatrix[i][j]=0;
						}
					}
					
				 int count=0;
				 
				 while(rset.next()) 
				 {
				 	
					String firstName = rset.getString("fname");
					String lastName = rset.getString("lname");
					String email = rset.getString("email");
				   availability = rset.getString("availability");
					std = new Student(firstName, lastName, email, availability);
					for(int i = 0; i < 7; i++)
					{ 
						for(int j=0; j < 12; j++)
						{
							finalMatrix[i][j]= finalMatrix[i][j] + std.getTime(i, j);
							}
						}
					
					//info to pass to update availability for each record
					out.println("<form action='updateAvail' method='POST'>");
					out.print(std.getfName() + " " + std.getlName() + ", " + std.getEmail() + " ");
					

					out.println("<input type='hidden' value='" + groupID + "' name='groupID' />");
					out.println("<input type='hidden' value='" + groupName + "' name='groupName' />");
					out.println("<input type='hidden' value='" + firstName + "' name='firstName' />");
					out.println("<input type='hidden' value='" + lastName + "' name='lastName' />"); 
					out.println("<input type='hidden' value='" + email + "' name='email' />");
					out.println("<input type='hidden' value='" + availability + "' name='times' />");
					out.println("<button type='submit' class='btn btn-mini btn-success' title='Update your availability'><i class='icon-white icon-time'></i></button>");
     				 			
					out.println("</form>");
						
					lastUserID = rset.getString("userID");
					count++;
						
				}
				//Display a form to add group member
				out.println("<br><h3>Enter your info to become a member of <span class='color-highlight2'>" + groupName +"!</span></h3>");
				out.println("<form name='memForm' action='insertAvail' onsubmit='return validateForm(memForm)' method='POST'>");
            out.println("<input type='text' class='input-large' name='firstName' placeholder='First Name'> <br>");
				out.println("<input type='text' class='input-large' name='lastName' placeholder='Last Name'> <br>");
				out.println("<input type='text' class='input-large' name='email' placeholder='Email'> <br>");
				out.println("<input type='hidden' value='" + groupID + "' name='groupID' />");
				out.println("<input type='hidden' value='" + groupName + "' name='groupName' />");
				out.println("<input type='hidden' value='" + availability + "' name='times' />");
				out.println("<input type='hidden' value='" + lastUserID + "' name='userID' />");
          	out.println("<button type='submit' class='btn btn-success'><i class='icon-white icon-user'></i>Join this group</button>");
        		out.println("</form><br>");

				
				int best = 0;
				String totalBest="";
				

				out.println("</div>"); //Ends the current members div
				boolean timesIntersect = false;
				//check to see if there are intersecting times
				for(int a = 0; a < 7; a++){
					for(int b = 0; b < 12; b++){
					if(finalMatrix[a][b] > 1)
					{
						timesIntersect = true;
					}
					}
				}
			//if no times are intersecting
			if(!timesIntersect)
			{
				out.println("<div class='span6'><center><h2>No best times to meet yet.</h2>");
				out.println("</center></div>");
				out.println("</div><br>");
			}
			else if(timesIntersect){
				for (int d = 0; d < 7; d++){
            	for (int t = 0; t < 12; t++){
                if (finalMatrix[d][t] > best){
                    //if the current value at finalMatrix is larger than best, then set that as the new best
                    best = finalMatrix[d][t];
                    //also, add to the string the day index, parsed to a char representing the day
                    //as well as the time, parsed to a string showing the time spans
                    totalBest = toDay(d) + " " + toTime(t);
                }
                //if finalMatrix < best do nothing, but if equal, add on to the string with a new location as an additional best time
                else if (finalMatrix[d][t] == best)
                    totalBest += "<br>" + toDay(d) + " " + toTime(t);
            }
       	  }
				//display of best available times
				out.println("<div class='span7'><center><h2>Best times to meet are: </h2><h4>" + totalBest);
				out.println("</h4>This will allow for " + best + " out of " + count + " members to attend.");
				//display table of time matrix
				out.println("<br><h5>This table shows how many members are available for each time slot.</h5>");
				out.println("<table class='table table-hover table-bordered table-condensed'><tr>");
				out.println("<th>Times</th>");
				out.println("<th>Monday</th>");
				out.println("<th>Tuesday</th>");
				out.println("<th>Wednesday</th>");
				out.println("<th>Thursday</th>");
				out.println("<th>Friday</th>");
				out.println("<th>Saturday</th>");
				out.println("<th>Sunday</th>");
				out.println("</tr>");
				out.println("<tr class='info'>");
				out.println("<td>9:00am - 10:00am</td>");
				for(int i=0; i < 7;i++)
				{
					out.println("<td>" + finalMatrix[i][0] + "</td>");
					}
				out.println("</tr>");
				out.println("<tr class='info'>");
				out.println("<td>10:00am - 11:00am</td>");
				for(int i=0; i < 7;i++)
				{
					out.println("<td>" + finalMatrix[i][1] + "</td>");
					}
				out.println("</tr>");
				out.println("<tr class='info'>");
				out.println("<td>11:00am - 12:00pm</td>"); 
				for(int i=0; i < 7;i++)
				{
					out.println("<td>" + finalMatrix[i][2] + "</td>");
					}
				out.println("                </tr>");
				out.println("                <tr class='info'>");
				out.println("                <td>12:00pm - 1:00pm</td>");
				for(int i=0; i < 7;i++)
				{
					out.println("<td>" + finalMatrix[i][3] + "</td>");
					}
				out.println("                </tr>");
				out.println("                <tr class='info'>");
				out.println("                <td>1:00pm - 2:00pm</td>");
				for(int i=0; i < 7;i++)
				{
					out.println("<td>" + finalMatrix[i][4] + "</td>");
					}
				out.println("               </tr>");
				out.println("                <tr class='info'>");
				out.println("                <td>2:00pm - 3:00pm</td>"); 
				for(int i=0; i < 7;i++)
				{
					out.println("<td>" + finalMatrix[i][5] + "</td>");
					}
				out.println("                </tr>");
				out.println("                <tr class='info'>");
				out.println("                <td>3:00pm - 4:00pm</td>"); 
				for(int i=0; i < 7;i++)
				{
					out.println("<td>" + finalMatrix[i][6] + "</td>");
					}
				out.println("                </tr>");
				out.println("                <tr class='info'>");
				out.println("                <td>4:00pm - 5:00pm</td>"); 
				for(int i=0; i < 7;i++)
				{
					out.println("<td>" + finalMatrix[i][7] + "</td>");
					}
				out.println("                </tr>");
				out.println("                <tr class='info'>");
				out.println("                <td>5:00pm - 6:00pm</td>"); 
				for(int i=0; i < 7;i++)
				{
					out.println("<td>" + finalMatrix[i][8] + "</td>");
					}
				out.println("                </tr>");
				out.println("                <tr class='info'>");
				out.println("                <td>6:00pm - 7:00pm</td>"); 
				for(int i=0; i < 7;i++)
				{
					out.println("<td>" + finalMatrix[i][9] + "</td>");
					}
				out.println("                </tr>");
				out.println("                <tr class='info'>");
				out.println("                <td>7:00pm - 8:00pm</td>"); 
				for(int i=0; i < 7;i++)
				{
					out.println("<td>" + finalMatrix[i][10] + "</td>");
					}
				out.println("                </tr>");
				out.println("                <tr class='info'>");
				out.println("                <td>8:00pm - 9:00pm</td>"); 
				for(int i=0; i < 7;i++)
				{
					out.println("<td>" + finalMatrix[i][11] + "</td>");
					}
				out.println("                </tr>");
				out.println("             </table>");
				//end table of times
				out.println("</center></div>");
				out.println("</div><br>");
			 }
				
      		out.println("<hr><div class='footer'>� Jacilyn Watts, Michael Piccot, Thomas Podhany, Nai Saevang, Garrett Reynolds, Anna Esteban, 2013<br><br></div>");
	         out.println("</body></html>"); //end of html code
					
			} catch (Exception e) 
			{
				e.printStackTrace();
				System.err.println("Exception: " + e.getMessage());
			} 
			finally 
			{
				try 
				{
					if (conn != null) 
					{
						conn.close();
					}
				} 
				catch (SQLException e)
				{
				}               
			}
		}
    }

public boolean groupExists(String id)
	{
		Connection conn2 = null;
		Statement stmt2 = null;
		boolean found = false;
		
		try
			{
				conn2 = DriverManager.getConnection("jdbc:mysql://localhost:8888/planit", "root", "dharma");
				stmt2 = conn2.createStatement();
				String search = "SELECT groupID FROM user WHERE groupID = " + "'" + id + "';";
				ResultSet rset = stmt2.executeQuery(search);
				if(rset.next())
				{
					found = true;
				}
				
			}catch (Exception e) 
			{
				e.printStackTrace();
				System.err.println("Exception: " + e.getMessage());
			} 
			finally 
			{
				try 
				{
					if (conn2 != null) 
					{
						conn2.close();
					}
				} 
				catch (SQLException e)
				{
				}               
			}
		return found;
	}
	
//this is straightforward, turns days indexes to chars and time indexes into string time spans
public static String toDay(int day){
        String convday;
        switch(day){
            case 0: convday = "Monday";
                break;
            case 1: convday = "Tuesday";
                break;
            case 2: convday = "Wednesday";
                break;
            case 3: convday = "Thursday";
                break;
            case 4: convday = "Friday";
                break;
            case 5: convday = "Saturday";
                break;
            case 6: convday = "Sunday";
                break;
            default: convday = "N/A";
                break;
        }
        return convday;
    }
	 
 public static String toTime(int time){
        String convtimea, convtimeb;
        switch(time){
            case 0: convtimea = "9";
					convtimeb = "10";
                break;
            case 1: convtimea = "10";
                convtimeb = "11";
                break;
            case 2: convtimea = "11";
                convtimeb = "12";
                break;
            case 3: convtimea = "12";
                convtimeb = "1";
                break;
            case 4: convtimea = "1";
                convtimeb = "2";
                break;
            case 5: convtimea = "2";
                convtimeb = "3";
                break;
            case 6: convtimea = "3";
                convtimeb = "4";
                break;
            case 7: convtimea = "4";
                convtimeb = "5";
                break;
            case 8: convtimea = "5";
                convtimeb = "6";
                break;
            case 9: convtimea = "6";
                convtimeb = "7";
                break;
            case 10: convtimea = "7";
                convtimeb = "8";
                break;
            case 11: convtimea = "8";
                convtimeb = "9";
                break;
            default: convtimea = "Invalid";
                convtimeb = "Invalid";
                break;
        }
        switch (time){
            case 0: case 1: convtimea += "AM-";
					convtimeb += "AM";
                break;
				case 2: convtimea += "AM-";
					convtimeb += "PM";
					break;
            case 3: case 4: case 5: case 6: case 7: case 8: case 9: case 10: case 11: convtimea += "PM-";
                convtimeb += "PM";
                break;
            default: convtimea += "Invalid";
                convtimeb += "Invalid";
                break;
					 
        }
        return convtimea + convtimeb;
    }

}

class Student{
    private String first, mail, last;
    private int[][] bestTime = new int[7][12];
    Student(){
    }
    Student(String fName, String lName, String email, String avail){
        first = fName;
        last = lName;
        mail = email;
        parsestring(avail);
    }
    //parse a string of 01 into the int matrix
    public void parsestring(String ava){
        for(int i = 0; i < 7; i++){
            for(int j = 0; j < 12; j++){
                bestTime[i][j] = Integer.parseInt(Character.toString(ava.charAt(i*12 + j)));
            }
        }
    }

    //if isGood is true, sets the flag in time array to 1, otherwise sets is to 0
    public void setTime(int day, int time, boolean isGood){
        int temp = 0;
        if (isGood)
            temp = 1;
        bestTime[day][time] = temp;
    }
    //returns specific 0/1 in a spot in time array
    public int getTime(int day, int time){
        return bestTime[day][time];
    }
    public String getfName(){
        return first;
    }
    public String getEmail(){
        return mail;
    }
    public String getlName(){
        return last;
    }
}


